﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Assignment4
{
    
    public class TicTacToe
    {
        #region Class Level Variables
        /// <summary>
        /// Boolean variable to keep track of which turn it is. False is for "X." True is for "O."
        /// </summary>
        Boolean boolCurrentTurn = false;
        /// <summary>
        /// Boolean variable to keep track of if the game has been won so that no more turns can be taken.
        /// </summary>
        Boolean boolIsWon = false;
        /// <summary>
        /// String array to keep track of the current state of the game board.
        /// </summary>
        public String[,] arrGameBoard = new String[3,3];
        #endregion

        #region Methods
        /// <summary>
        /// This method is the public access to run a turn. It receives an image object and passes it to 
        /// be clicked.
        /// </summary>
        /// <param name="imClickedImage">The image to be clicked for the turn.</param>
        public void runTurn(Image imClickedImage)
        {
            if (boolIsWon == false)
            {
                if (isClicked(imClickedImage) == false)
                    clickBox(imClickedImage);
            }
        }

        /// <summary>
        /// Checks arrGameBoard to see if the current selection has already been clicked. Returns true if it has been. Returns false if not.
        /// </summary>
        /// <param name="imClickedImage">The image to check.</param>
        /// <returns></returns>
        private bool isClicked(Image imClickedImage)
        {   
            switch (imClickedImage.Name)
            {
                case "imBox11":
                    if (arrGameBoard[0, 0] != null)
                        return true;
                    else
                        return false;                   
                case "imBox12":
                    if (arrGameBoard[0, 1] != null)
                        return true;
                    else
                        return false;
                case "imBox13":
                    if (arrGameBoard[0, 2] != null)
                        return true;
                    else
                        return false;
                case "imBox21":
                    if (arrGameBoard[1, 0] != null)
                        return true;
                    else
                        return false;
                case "imBox22":
                    if (arrGameBoard[1, 1] != null)
                        return true;
                    else
                        return false;
                case "imBox23":
                    if (arrGameBoard[1, 2] != null)
                        return true;
                    else
                        return false;
                case "imBox31":
                    if (arrGameBoard[2, 0] != null)
                        return true;
                    else
                        return false;
                case "imBox32":
                    if (arrGameBoard[2, 1] != null)
                        return true;
                    else
                        return false;
                case "imBox33":
                    if (arrGameBoard[2, 2] != null)
                        return true;
                    else
                        return false;
            }
            return false;
        }

        /// <summary>
        /// This method sets the image passed in to a picture based off of the current turn, calls markArray based off of the turn, and switches the current turn.
        /// </summary>
        /// <param name="imClickedImage">The imaged to be changed.</param>
        private void clickBox(Image imClickedImage)
        {
            if (boolCurrentTurn == false)
            {
                //lbClickedLabel.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFF1F1F1"));
                //lbClickedLabel.Content = "X";
                imClickedImage.Source = new BitmapImage(new Uri(@"\Charizard.png", UriKind.RelativeOrAbsolute));
                imClickedImage.Opacity = 100;
                markArray(imClickedImage, "X");
                boolCurrentTurn = true;
            }
            else
            {
                //lbClickedLabel.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFF1F1F1"));
                //lbClickedLabel.Content = "O";
                imClickedImage.Source = new BitmapImage(new Uri(@"\Blastoise.png", UriKind.Relative));
                imClickedImage.Opacity = 100;
                markArray(imClickedImage, "O");
                boolCurrentTurn = false;
            }
        }

        /// <summary>
        /// Marks arrGameBoard at the given spot for the turn.
        /// </summary>
        /// <param name="imClickedImage">The image that has been clicked.</param>
        /// <param name="stChoice">The choice to mark on the array.</param>
        private void markArray(Image imClickedImage, string stChoice)
        {
            switch (imClickedImage.Name)
            {
                case "imBox11":
                    arrGameBoard[0, 0] = stChoice;
                    break;
                case "imBox12":
                    arrGameBoard[0, 1] = stChoice;
                    break;
                case "imBox13":
                    arrGameBoard[0, 2] = stChoice;
                    break;
                case "imBox21":
                    arrGameBoard[1, 0] = stChoice;
                    break;
                case "imBox22":
                    arrGameBoard[1, 1] = stChoice;
                    break;
                case "imBox23":
                    arrGameBoard[1, 2] = stChoice;
                    break;
                case "imBox31":
                    arrGameBoard[2, 0] = stChoice;
                    break;
                case "imBox32":
                    arrGameBoard[2, 1] = stChoice;
                    break;
                case "imBox33":
                    arrGameBoard[2, 2] = stChoice;
                    break;
            }
        }

        /// <summary>
        /// Runs check methods for all rows and columns to determine if there is a win and returns the corresponding row/column.
        /// </summary>
        /// <returns></returns>
        public string isWin()
        {
            
            if (row1Win() == true)
            {
                boolIsWon = true;
                return "row1";
            }
            if (row2Win() == true)
            {
                boolIsWon = true;
                return "row2";
            }
            if (row3Win() == true)
            {
                boolIsWon = true;
                return "row3";
            }
            if (col1Win() == true)
            {
                boolIsWon = true;
                return "col1";
            }
            if (col2Win() == true)
            {
                boolIsWon = true;
                return "col2";
            }
            if (col3Win() == true)
            {
                boolIsWon = true;
                return "col3";
            }
            if (diag1Win() == true)
            {
                boolIsWon = true;
                return "diag1";
            }
            if (diag2Win() == true)
            {
                boolIsWon = true;
                return "diag2";
            }
            foreach (string stValue in arrGameBoard)
            {
                if (stValue == null) { return null; }
            }
            return "tie";
        }

        /// <summary>
        /// Checks to see which turn just went to determine who won and returns that as a string.
        /// </summary>
        /// <returns></returns>
        public string whoWon()
        {
            if(boolCurrentTurn == true)
            {
                return "X";
            }
            else
            {
                return "O";
            }
        }

        /// <summary>
        /// Checks to see if there is a win in row 1.
        /// </summary>
        /// <returns></returns>
        private bool row1Win()
        {
            if ((arrGameBoard[0, 0] == "X" && arrGameBoard[0, 1] == "X" && arrGameBoard[0, 2] == "X") ||
                (arrGameBoard[0, 0] == "O" && arrGameBoard[0, 1] == "O" && arrGameBoard[0, 2] == "O"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks to see if there is a win in row 2.
        /// </summary>
        /// <returns></returns>
        private bool row2Win()
        {
            if ((arrGameBoard[1, 0] == "X" && arrGameBoard[1, 1] == "X" && arrGameBoard[1, 2] == "X") ||
                (arrGameBoard[1, 0] == "O" && arrGameBoard[1, 1] == "O" && arrGameBoard[1, 2] == "O"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks to see if there is a win in row 3.
        /// </summary>
        /// <returns></returns>
        private bool row3Win()
        {
            if ((arrGameBoard[2, 0] == "X" && arrGameBoard[2, 1] == "X" && arrGameBoard[2, 2] == "X") ||
                (arrGameBoard[2, 0] == "O" && arrGameBoard[2, 1] == "O" && arrGameBoard[2, 2] == "O"))
            {           
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks to see if there is a win in column 1.
        /// </summary>
        /// <returns></returns>
        private bool col1Win()
        {
            if ((arrGameBoard[0, 0] == "X" && arrGameBoard[1, 0] == "X" && arrGameBoard[2, 0] == "X") ||
                (arrGameBoard[0, 0] == "O" && arrGameBoard[1, 0] == "O" && arrGameBoard[2, 0] == "O"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks to see if there is a win in column 2.
        /// </summary>
        /// <returns></returns>
        private bool col2Win()
        {
            if ((arrGameBoard[0, 1] == "X" && arrGameBoard[1, 1] == "X" && arrGameBoard[2, 1] == "X") ||
                (arrGameBoard[0, 1] == "O" && arrGameBoard[1, 1] == "O" && arrGameBoard[2, 1] == "O"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks to see if there is a win in column 3.
        /// </summary>
        /// <returns></returns>
        private bool col3Win()
        {
            if ((arrGameBoard[0, 2] == "X" && arrGameBoard[1, 2] == "X" && arrGameBoard[2, 2] == "X") ||
                (arrGameBoard[0, 2] == "O" && arrGameBoard[1, 2] == "O" && arrGameBoard[2, 2] == "O"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks to see if there is a win in the first diagonal. 
        /// </summary>
        /// <returns></returns>
        private bool diag1Win()
        {
            if ((arrGameBoard[0, 0] == "X" && arrGameBoard[1, 1] == "X" && arrGameBoard[2, 2] == "X") ||
                (arrGameBoard[0, 0] == "O" && arrGameBoard[1, 1] == "O" && arrGameBoard[2, 2] == "O"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks to see if there is a win in the second diagonal. 
        /// </summary>
        /// <returns></returns>
        private bool diag2Win()
        {
            if ((arrGameBoard[0, 2] == "X" && arrGameBoard[1, 1] == "X" && arrGameBoard[2, 0] == "X") ||
                (arrGameBoard[0, 2] == "O" && arrGameBoard[1, 1] == "O" && arrGameBoard[2, 0] == "O"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Resets all values to what they were initialized to.
        /// </summary>
        public void resetGame()
        {
            for (int i = 0; i < 3; i++)
                for(int j = 0; j < 3; j++)
                    arrGameBoard[i,j] = null;
            boolCurrentTurn = false;
            boolIsWon = false;
        }
        #endregion
    }

    public partial class MainWindow : Window
    {
        #region Class Level Variables
        /// <summary>
        /// An instance of the TicTacToe class to use for the main window. 
        /// </summary>
        TicTacToe game = new TicTacToe();
        /// <summary>
        /// Integer to keep track of player 1 wins.
        /// </summary>
        int iPlayer1Wins = 0;
        /// <summary>
        /// Integer to keep track of player 2 wins.
        /// </summary>
        int iPlayer2Wins = 0;
        /// <summary>
        /// Integer to keep track of number of ties.
        /// </summary>
        int iTies = 0;
        /// <summary>
        /// Integer to keep track of how many turns have been taken.
        /// </summary>
        int iTurn = 0;
        /// <summary>
        /// Boolean to determine if player 2 is an AI player or not.
        /// </summary>
        Boolean boolPlayingAi = false;
        /// <summary>
        /// Boolean to determine if game has been started so that AI cannot be activated mid game.
        /// </summary>
        Boolean boolGameRunning = false;
        /// <summary>
        /// Image array to keep track of the board so AI can pass Images to be used for a turn.
        /// </summary>
        Image[,] arrBoxes;
        #endregion

        #region Methods
        /// <summary>
        /// Initalizes main window and arrBoxes.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            arrBoxes = new Image[3,3] { { imBox11, imBox12, imBox13 },
                                        { imBox21, imBox22, imBox23 },
                                        { imBox31, imBox32, imBox33 }};
        }

        /// <summary>
        /// When an image is clicked, it sets the boolGameRunning to true, passes the selected image to 
        /// be used in a turn, runs an AI turn if necessary, and checks for a win.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbBox_MouseUp(object sender, MouseButtonEventArgs e)
        {
            boolGameRunning = true;

            Image imSelected = (Image)sender;

            game.runTurn(imSelected);

            if(boolPlayingAi == true)
                aiPlayer();

            gameWon();

           
        }

        /// <summary>
        /// Determines if there was a win and highlights the win.
        /// </summary>
        public void gameWon()
        {
            switch (game.isWin())
            {
                case "row1":
                    recBox11.Opacity = 100;
                    recBox12.Opacity = 100;
                    recBox13.Opacity = 100;
                    boolGameRunning = false;
                    displayWin();
                    break;
                case "row2":
                    recBox21.Opacity = 100;
                    recBox22.Opacity = 100;
                    recBox23.Opacity = 100;
                    boolGameRunning = false;
                    displayWin();
                    break;
                case "row3":
                    recBox31.Opacity = 100;
                    recBox32.Opacity = 100;
                    recBox33.Opacity = 100;
                    boolGameRunning = false;
                    displayWin();
                    break;
                case "col1":
                    recBox11.Opacity = 100;
                    recBox21.Opacity = 100;
                    recBox31.Opacity = 100;
                    boolGameRunning = false;
                    displayWin();
                    break;
                case "col2":
                    recBox12.Opacity = 100;
                    recBox22.Opacity = 100;
                    recBox32.Opacity = 100;
                    boolGameRunning = false;
                    displayWin();
                    break;
                case "col3":
                    recBox13.Opacity = 100;
                    recBox23.Opacity = 100;
                    recBox33.Opacity = 100;
                    boolGameRunning = false;
                    displayWin();
                    break;
                case "diag1":
                    recBox11.Opacity = 100;
                    recBox22.Opacity = 100;
                    recBox33.Opacity = 100;
                    boolGameRunning = false;
                    displayWin();
                    break;
                case "diag2":
                    recBox13.Opacity = 100;
                    recBox22.Opacity = 100;
                    recBox31.Opacity = 100;
                    boolGameRunning = false;
                    displayWin();
                    break;
                case "tie":
                    iTies++;
                    lbTies.Content = iTies;
                    tbDisplayOutcome.Text = "Tied game. No one wins.";
                    boolGameRunning = false;
                    break;
            }
        }

        /// <summary>
        /// Displays the win in tbDisplayOutcome, updates and displays the statistics for the window. 
        /// </summary>
        public void displayWin()
        {
            if (game.whoWon() == "X")
            {
                iPlayer1Wins++;
                lbPlayer1Wins.Content = iPlayer1Wins;
                tbDisplayOutcome.Text = "Player 1 Wins";
            }
            if (game.whoWon() == "O")
            {
                iPlayer2Wins++;
                lbPlayer2Wins.Content = iPlayer2Wins;
                tbDisplayOutcome.Text = "Player 2 Wins";
            }
        }

        /// <summary>
        /// Resets the game to it's original state so it can be played again.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btStartNextGame_Click(object sender, RoutedEventArgs e)
        {
            game.resetGame();
            imBox11.Opacity = 0;
            imBox12.Opacity = 0;
            imBox13.Opacity = 0;
            imBox21.Opacity = 0;
            imBox22.Opacity = 0;
            imBox23.Opacity = 0;
            imBox31.Opacity = 0;
            imBox32.Opacity = 0;
            imBox33.Opacity = 0;
            recBox11.Opacity = 0;
            recBox12.Opacity = 0;
            recBox13.Opacity = 0;
            recBox21.Opacity = 0;
            recBox22.Opacity = 0;
            recBox23.Opacity = 0;
            recBox31.Opacity = 0;
            recBox32.Opacity = 0;
            recBox33.Opacity = 0;
            tbDisplayOutcome.Text = "";
            iTurn = 0;
        }

        /// <summary>
        /// Manages each turn for the AI player and passes Images to be used for turns.
        /// </summary>
        public void aiPlayer()
        {


            switch (iTurn)
            {
                case 0:
                    if (game.arrGameBoard[1, 1] == null)
                        game.runTurn(imBox22);
                    else
                        game.runTurn(imBox11);
                    iTurn++;
                    break;
                case 1:
                    if (aiCheckBlock() != null)
                    {
                        game.runTurn(aiCheckBlock());
                        iTurn++;
                        break;
                    }
                    turn2();
                    iTurn++;
                    break;
                case 2:
                    if (aiCheckWin() != null)
                    {
                        game.runTurn(aiCheckWin());
                        break;
                    }

                    if (aiCheckBlock() != null)
                    {
                        game.runTurn(aiCheckBlock());
                        break;
                    }

                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            if (game.arrGameBoard[i, j] == null)
                            {
                                game.runTurn(arrBoxes[i, j]);
                                return;
                            }
                    break;
            }

            
        }

        /// <summary>
        /// Determines what the AI will do on it's second turn. Second turn is essential for 
        /// either tying or winning game for player 2, so it has it's own method.
        /// </summary>
        public void turn2()
        {
            if (game.arrGameBoard[0, 0] == "X" && game.arrGameBoard[2, 1] == "X")
            {
                game.runTurn(imBox31);
                return;
            }
            if (game.arrGameBoard[0, 0] == "X" && game.arrGameBoard[1, 2] == "X")
            {
                game.runTurn(imBox13);
                return;
            }
            if (game.arrGameBoard[0, 2] == "X" && game.arrGameBoard[1, 0] == "X")
            {
                game.runTurn(imBox11);
                return;
            }
            if (game.arrGameBoard[0, 2] == "X" && game.arrGameBoard[2, 1] == "X")
            {
                game.runTurn(imBox33);
                return;
            }
            if (game.arrGameBoard[2, 0] == "X" && game.arrGameBoard[0, 1] == "X")
            {
                game.runTurn(imBox11);
                return;
            }
            if (game.arrGameBoard[2, 0] == "X" && game.arrGameBoard[1, 2] == "X")
            {
                game.runTurn(imBox33);
                return;
            }
            if (game.arrGameBoard[2, 2] == "X" && game.arrGameBoard[1, 0] == "X")
            {
                game.runTurn(imBox31);
                return;
            }
            if (game.arrGameBoard[2, 2] == "X" && game.arrGameBoard[0, 1] == "X")
            {
                game.runTurn(imBox13);
                return;
            }
            if (game.arrGameBoard[0, 1] == "X" && game.arrGameBoard[1, 0] == "X")
            {
                game.runTurn(imBox11);
                return;
            }
            if (game.arrGameBoard[0, 1] == "X" && game.arrGameBoard[1, 2] == "X")
            {
                game.runTurn(imBox13);
                return;
            }
            if (game.arrGameBoard[2, 1] == "X" && game.arrGameBoard[1, 2] == "X")
            {
                game.runTurn(imBox33);
                return;
            }
            if (game.arrGameBoard[2, 1] == "X" && game.arrGameBoard[1, 0] == "X")
            {
                game.runTurn(imBox31);
                return;
            }
            if (game.arrGameBoard[1, 1] == "X" && game.arrGameBoard[2, 2] == "X")
            {
                game.runTurn(imBox13);
                return;
            }
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    if (game.arrGameBoard[i, j] == null)
                    {
                        game.runTurn(arrBoxes[i, j]);
                        return;
                    }

        }

        /// <summary>
        /// Checks the board to see if there is a move that needs to be made to block a win for player 1.
        /// </summary>
        /// <returns></returns>
        public Image aiCheckBlock()
        {
            int iRow1 = 0;
            int iRow2 = 0;
            int iRow3 = 0;
            int iCol1 = 0;
            int iCol2 = 0;
            int iCol3 = 0;
            int iDiag1 = 0;
            int iDiag2 = 0;

            for(int i = 0; i < 3; i++)
            {
                if(game.arrGameBoard[0, i] == "X")
                    iRow1++;
                if (game.arrGameBoard[0, i] == "O")
                    iRow1--;
                if (game.arrGameBoard[1, i] == "X")
                    iRow2++;
                if (game.arrGameBoard[1, i] == "O")
                    iRow2--;
                if (game.arrGameBoard[2, i] == "X")
                    iRow3++;
                if (game.arrGameBoard[2, i] == "O")
                    iRow3--;
                if (game.arrGameBoard[i, 0] == "X")
                    iCol1++;
                if (game.arrGameBoard[i, 0] == "O")
                    iCol1--;
                if (game.arrGameBoard[i, 1] == "X")
                    iCol2++;
                if (game.arrGameBoard[i, 1] == "O")
                    iCol2--;
                if (game.arrGameBoard[i, 2] == "X")
                    iCol3++;
                if (game.arrGameBoard[i, 2] == "O")
                    iCol3--;
            }

            if (game.arrGameBoard[0, 0] == "X")
                iDiag1++;
            if (game.arrGameBoard[0, 0] == "O")
                iDiag1--;
            if (game.arrGameBoard[1, 1] == "X")
                iDiag1++;
            if (game.arrGameBoard[1, 1] == "O")
                iDiag1--;
            if (game.arrGameBoard[2, 2] == "X")
                iDiag1++;
            if (game.arrGameBoard[2, 2] == "O")
                iDiag1--;
            if (game.arrGameBoard[2, 0] == "X")
                iDiag2++;
            if (game.arrGameBoard[2, 0] == "O")
                iDiag2--;
            if (game.arrGameBoard[1, 1] == "X")
                iDiag2++;
            if (game.arrGameBoard[1, 1] == "O")
                iDiag2--;
            if (game.arrGameBoard[0, 2] == "X")
                iDiag2++;
            if (game.arrGameBoard[0, 2] == "O")
                iDiag2--;

            if(iRow1 == 2)
            {
                for(int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[0, i] == null)
                        return arrBoxes[0, i];
                }
            }
            if (iRow2 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[1, i] == null)
                        return arrBoxes[1, i];
                }
            }
            if (iRow3 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[2, i] == null)
                        return arrBoxes[2, i];
                }
            }
            if (iCol1 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[i, 0] == null)
                        return arrBoxes[i, 0];
                }
            }
            if (iCol2 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[i, 1] == null)
                        return arrBoxes[i, 1];
                }
            }
            if (iCol3 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[i, 2] == null)
                        return arrBoxes[i, 2];
                }
            }
            if (iDiag1 == 2)
            {
                if (game.arrGameBoard[0, 0] == null)
                    return arrBoxes[0, 0];
                if (game.arrGameBoard[1, 1] == null)
                    return arrBoxes[1, 1];
                if (game.arrGameBoard[2, 2] == null)
                    return arrBoxes[2, 2];
            }
            if (iDiag2 == 2)
            {
                if (game.arrGameBoard[2, 0] == null)
                    return arrBoxes[2, 0];
                if (game.arrGameBoard[1, 1] == null)
                    return arrBoxes[1, 1];
                if (game.arrGameBoard[0, 2] == null)
                    return arrBoxes[0, 2];
            }

            return null;
        }

        /// <summary>
        /// Checks the board to see if a winning move can be made.
        /// </summary>
        /// <returns></returns>
        public Image aiCheckWin()
        {
            int iRow1 = 0;
            int iRow2 = 0;
            int iRow3 = 0;
            int iCol1 = 0;
            int iCol2 = 0;
            int iCol3 = 0;
            int iDiag1 = 0;
            int iDiag2 = 0;

            for (int i = 0; i < 3; i++)
            {
                if (game.arrGameBoard[0, i] == "O")
                    iRow1++;
                if (game.arrGameBoard[0, i] == "X")
                    iRow1--;
                if (game.arrGameBoard[1, i] == "O")
                    iRow2++;
                if (game.arrGameBoard[1, i] == "X")
                    iRow2--;
                if (game.arrGameBoard[2, i] == "O")
                    iRow3++;
                if (game.arrGameBoard[2, i] == "X")
                    iRow3--;
                if (game.arrGameBoard[i, 0] == "O")
                    iCol1++;
                if (game.arrGameBoard[i, 0] == "X")
                    iCol1--;
                if (game.arrGameBoard[i, 1] == "O")
                    iCol2++;
                if (game.arrGameBoard[i, 1] == "X")
                    iCol2--;
                if (game.arrGameBoard[i, 2] == "O")
                    iCol3++;
                if (game.arrGameBoard[i, 2] == "X")
                    iCol3--;
            }

            if (game.arrGameBoard[0, 0] == "O")
                iDiag1++;
            if (game.arrGameBoard[0, 0] == "X")
                iDiag1--;
            if (game.arrGameBoard[1, 1] == "O")
                iDiag1++;
            if (game.arrGameBoard[1, 1] == "X")
                iDiag1--;
            if (game.arrGameBoard[2, 2] == "O")
                iDiag1++;
            if (game.arrGameBoard[2, 2] == "X")
                iDiag1--;
            if (game.arrGameBoard[2, 0] == "O")
                iDiag2++;
            if (game.arrGameBoard[2, 0] == "X")
                iDiag2--;
            if (game.arrGameBoard[1, 1] == "O")
                iDiag2++;
            if (game.arrGameBoard[1, 1] == "X")
                iDiag2--;
            if (game.arrGameBoard[0, 2] == "O")
                iDiag2++;
            if (game.arrGameBoard[0, 2] == "X")
                iDiag2--;

            if (iRow1 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[0, i] == null)
                        return arrBoxes[0, i];
                }
            }
            if (iRow2 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[1, i] == null)
                        return arrBoxes[1, i];
                }
            }
            if (iRow3 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[2, i] == null)
                        return arrBoxes[2, i];
                }
            }
            if (iCol1 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[i, 0] == null)
                        return arrBoxes[i, 0];
                }
            }
            if (iCol2 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[i, 1] == null)
                        return arrBoxes[i, 1];
                }
            }
            if (iCol3 == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (game.arrGameBoard[i, 2] == null)
                        return arrBoxes[i, 2];
                }
            }
            if (iDiag1 == 2)
            {
                if (game.arrGameBoard[0, 0] == null)
                    return arrBoxes[0, 0];
                if (game.arrGameBoard[1, 1] == null)
                    return arrBoxes[1, 1];
                if (game.arrGameBoard[2, 2] == null)
                    return arrBoxes[2, 2];
            }
            if (iDiag2 == 2)
            {
                if (game.arrGameBoard[2, 0] == null)
                    return arrBoxes[2, 0];
                if (game.arrGameBoard[1, 1] == null)
                    return arrBoxes[1, 1];
                if (game.arrGameBoard[0, 2] == null)
                    return arrBoxes[0, 2];
            }

            return null;
        }

        /// <summary>
        /// Allows switching between a person being player 2 or the AI being player 2.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btChangePlayer2_Click(object sender, RoutedEventArgs e)
        {
            if(boolPlayingAi == false && boolGameRunning == false)
            {
                boolPlayingAi = true;
                btChangePlayer2.Content = "Turn off AI";
            }
            else if(boolPlayingAi == true && boolGameRunning == false)
            {
                boolPlayingAi = false ;
                btChangePlayer2.Content = "Play Against AI";
            }
        }
        #endregion
    }


}
